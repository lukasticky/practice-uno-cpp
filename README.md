# not UNO (C++)

A simple UNO game for exercise; It's not fun to play, but more of a proof of concept.

## TODO

- Player:
    - implement `std::find` with Objects
- Game:
    - skip, draw, reverse (direction)

## Documentation

### Build & Run

```sh
$ make
```

```
$ ./UNO
```

### Classes

| Class       | Description                          |
|:------------|:-------------------------------------|
| Menu        | (Play/Quit) and getting player names |
| Game        | Combines all classes and game loop   |
| RenderCLI   | "Renders" game in CLI                |
| Player      | has name and pile of cards           |
| Card        | Value, Color, etc.                   |

## Disclaimer

Not affiliated with Mattel and UNO yada yada