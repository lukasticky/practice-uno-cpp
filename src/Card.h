#pragma once

#include <map>
#include <string>

struct Card {
public:
    enum class Color { Wildcard = -1, Red, Blue, Yellow, Green };

    // constructor & destructor
    Card(int value, Color color, int draw, bool skip, bool reverse, bool isWildcard = false) :
        _value(value), _color(color), _draw(draw), _skip(skip), _reverse(reverse), _isWildcard(isWildcard) {}
    ~Card() {}

    // getters
    std::string GetCardString();
    int GetValue();
    Color GetColor();
    int GetDraw();
    bool GetSkip();
    bool GetReverse();
    bool IsWildcard();

private:
    int _value; // -1 = none
    Color _color; // -1 = none/wildcard, 0 = red, 1 = blue, 2 = yellow, 3 = green
    int _draw; // how many cards need to be drawn
    bool _skip;
    bool _reverse;
    bool _isWildcard;
};