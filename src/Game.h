#pragma once

#include <algorithm>
#include <random>
#include <ctime>
#include <vector>

#include "Card.h"
#include "Player.h"
#include "RenderCLI.h"

class Game {
public:
    Game(std::vector<Player> players);
    ~Game();

private:
    enum class DeckType { Shuffle = 0, Standard };
    DeckType _decktype = DeckType::Standard;

    // render
    RenderCLI render;

    // players
    std::vector<Player> _players;
    Player* _currPlayer;
    
    // cards
    std::vector<Card> _drawPile;
    std::vector<Card> _discardPile;

    // gameplay
    int direction = 1;
    void PlayCard(Card* card);

    // generate a shuffled deck according to `_decktype`
    std::vector<Card> GenerateDeck();

    // generators
    void GenerateDeckStandard(std::vector<Card> * cards);

};