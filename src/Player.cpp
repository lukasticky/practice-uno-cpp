#include "Player.h"

std::string Player::GetName() {
    return _name;
}

std::vector<Card> Player::GetCards() {
    return _cards;
}

void Player::AddCard(Card card) {
    _cards.push_back(card);
}

void Player::PopCard(Card card) {
    auto it = std::find(_cards.begin(), _cards.end(), card);
    if (it != _cards.end())
    _cards.erase(it);
}