#include "Game.h"

Game::Game(std::vector<Player> players) {
    // setup
    char input;
    _players = players;
    _currPlayer = &_players.at(0);
    _drawPile = GenerateDeck();

    // hand out 7 cards to every player
    for (auto it = std::begin(_players); it != std::end(_players); ++it) {
        for (int i = 0; i < 7; i++) {
            it->AddCard(_drawPile.back());
            _drawPile.pop_back();
        }
    }

    // flip first card
    _discardPile.push_back(_drawPile.back());
    _drawPile.pop_back();

    // game loop
    while (true) {
        render.ReRender(_currPlayer, &_discardPile.back());

        // await input
        while (true) {
            std::cin >> input;
            int input_i = (int)input - 48;

            if (input_i == 0) {
                // player wants to draw card
                _currPlayer->AddCard(_drawPile.back());
                _drawPile.pop_back();
            } else if (input_i > 0 && input_i <= _currPlayer->GetCards().size()) {
                
                /* 
                 * Check if move is legal:
                 *  wildcard OR colors match OR (values match AND value > 1)
                 */

                if (_currPlayer->GetCards().at(input_i - 1).IsWildcard() || _currPlayer->GetCards().at(input_i - 1).GetColor() == _discardPile.back().GetColor() || (_currPlayer->GetCards().at(input_i - 1).GetValue() == _discardPile.back().GetValue() && _currPlayer->GetCards().at(input_i - 1).GetValue() > 1)) {
                    
                    _discardPile.push_back(_currPlayer->GetCards().at(input_i - 1));
                    _currPlayer->PopCard(_currPlayer->GetCards().at(input_i - 1));

                    break;
                } else {
                    std::cout << "E: Illegal Move\n\n> ";
                }

            } else {
                std::cout << "E: Illegal Option\n\n> ";
            }
        }
    }
}

Game::~Game() {

}

std::vector<Card> Game::GenerateDeck() {
    std::vector<Card> cards;

    switch (_decktype) {
        case DeckType::Standard:
            Game::GenerateDeckStandard(&cards);
            break;
        default:
            break;
    }

    // shuffle
    std::srand((std::time(0)));
    std::random_shuffle(std::begin(cards), std::end(cards));

    return cards;
}

void Game::GenerateDeckStandard(std::vector<Card> * cards) {
    /* 
     * Standard UNO Deck:
     *  - For each color:
     *      - 0
     *      - 2x [1-9]
     *      - 2x skip, 2x reverse, 2x draw 2
     *  - 4x wildcard
     *  - 4x wildcard + draw 4
     */

    // generate number cards
    for (int i = 0; i <= 9; i++)  {
        for (int j = 0; j < 4; j++) {
            cards->push_back(Card(i, Card::Color(j), 1, false, false));
            if (i == 0) {
                break; // only add twice if not 0
            }
            cards->push_back(Card(i, Card::Color(j), 1, false, false));
        }
    }

    // generate skip, reverse, draw 2
    for (int j = 0; j < 4; j++) {
        // skip
        cards->push_back(Card(-1, Card::Color(j), 1, true, false));
        cards->push_back(Card(-1, Card::Color(j), 1, true, false));
        // reverse
        cards->push_back(Card(-1, Card::Color(j), 1, false, true));
        cards->push_back(Card(-1, Card::Color(j), 1, false, true));
        // draw 2
        cards->push_back(Card(-1, Card::Color(j), 2, true, false));
        cards->push_back(Card(-1, Card::Color(j), 2, true, false));
    }

    // generate wildcards
    for (int i = 0; i < 4; i++) {
        // wildcard
        cards->push_back(Card(-1, Card::Color::Wildcard, 1, false, false, true));
        // wildcard + draw 4
        cards->push_back(Card(-1, Card::Color::Wildcard, 4, false, false, true));
    }
}