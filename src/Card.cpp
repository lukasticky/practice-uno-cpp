#include "Card.h"

std::string Card::GetCardString() {
    std::string color;

    // set `color`-string according to `Color`
    switch (_color) {
        case Color::Wildcard:
            color = "Wildcard";
            break;
        case Color::Red:
            color = "RED";
            break;
        case Color::Blue:
            color = "BLUE";
            break;
        case Color::Yellow:
            color = "YELLOW";
            break;
        case Color::Green:
            color = "GREEN";
            break;
    }
    
    // generate final card string
    if (_value >= 0) {
        // number card
        return (color + " " + std::to_string(_value));
    } else if (_isWildcard) {
        // wildcard
        if (_draw > 1) {
            if (_color != Color::Wildcard) {
                return (color + " Draw " + std::to_string(_draw) + " Wildcard");
            } else {
                return ("Draw " + std::to_string(_draw) + " Wildcard");
            }
        } else {
            if (_color != Color::Wildcard) {
                return (color + " Wildcard");
            } else {
                return "Wildcard";
            }
        }
    } else {
        // special color cards
        if (_skip) {
            if (_draw > 1) {
                return (color + " Draw " + std::to_string(_draw));
            }
            return (color + " Skip");
        }
        if (_reverse) {
            return (color + " Reverse");
        }
    }
    return color;
}

int Card::GetValue() {
    return _value;
}

Card::Color Card::GetColor() {
    return _color;
}

int Card::GetDraw() {
    return _draw;
}

bool Card::GetSkip() {
    return _skip;
}

bool Card::GetReverse() {
    return _reverse;
}

bool Card::IsWildcard() {
    return _isWildcard;
}