#pragma once

#include <iostream>
#include <string>

#include "Card.h"
#include "Player.h"

class RenderCLI {
public:
    RenderCLI();
    ~RenderCLI();

    void Render(Player* player, Card* discardCard);
    void ReRender(Player* player, Card* discardCard);
    void Clear();
};