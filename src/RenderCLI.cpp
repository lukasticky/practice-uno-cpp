#include "RenderCLI.h"

RenderCLI::RenderCLI() {

}

RenderCLI::~RenderCLI() {

}

void RenderCLI::Render(Player* player, Card* discardCard) {
    std::cout << "==================================================\n\
It's your turn, " << player->GetName() << "\n\
--------------------------------------------------\n\
Discard Pile: " << discardCard->GetCardString() << "\n\
--------------------------------------------------\n\n\
Choose a card to play:\n\n\
 [0] Draw card from Discard Pile\n\n";

    // print player cards
    for (int i = 0; i < player->GetCards().size(); i++) {
        std::cout << " [" << i + 1 << "] " << player->GetCards()[i].GetCardString() << "\n";
    }
    std::cout << "\n> ";
}

void RenderCLI::ReRender(Player* player, Card* discardCard) {
    Clear();
    Render(player, discardCard);
}

void RenderCLI::Clear() {
    #ifdef _WIN32
        system("cls");
    #else
        system("clear");
    #endif
}