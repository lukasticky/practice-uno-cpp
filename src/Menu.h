#pragma once

#include <iostream>
#include <vector>

#include "Game.h"
#include "Player.h"

class Interface {
public:
    Interface();
    ~Interface();

private:
    enum Option { Play = 0, Quit = 1 };
    
    char _input;
    Option _option;
    int _playerAmt;
    std::vector<Player> _players;

    void MainMenu();
    void PlayerMenu();
};