#include "Menu.h"

Interface::Interface() {
    MainMenu();
    if (_option == Play) {
        PlayerMenu();
        Game game(_players);
    }
}

Interface::~Interface() {

}

void Interface::MainMenu() {
    while (true) {

        // print main menu
        std::cout << "@  @  @  @   @@ \n\
@  @  @@ @  @  @\n\
@  @  @ @@  @  @\n\
 @@   @  @   @@\n\
\n\
 [1] Play\n [2] Quit\n\n> ";

        std::cin >> _input;
        std::cout << std::endl;

        switch (_input) {
            case '1':
                _option = Play;
                return;
            case '2':
                _option = Quit;
                return;
            default: break;
        }
    }
}

void Interface::PlayerMenu() {
    std::cout << "How many players are there?\n> ";
    std::cin >> _playerAmt;

    for (int i = 0; i < _playerAmt; i++) {
        std::string name;
        std::cout << "Player " << i + 1 << ", what's your name?\n> ";
        std::cin >> name;
        _players.push_back(Player(name));
    }
}