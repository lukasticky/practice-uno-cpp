#pragma once

#include <algorithm>
#include <string>
#include <vector>

#include "Card.h"

class Player {
public:
    Player(std::string name) {
        _name = name;
    }
    ~Player() {}

    // getters
    std::string GetName();
    std::vector<Card> GetCards();

    // deck modifiers
    void AddCard(Card card);
    void PopCard(Card card);

private:
    std::string _name;
    std::vector<Card> _cards;
};